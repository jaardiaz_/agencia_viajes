# README #
### What is this repository for? ###

* Repositorio que contiene tanto el frontend como el backend del proyecto "Agencia de viajes" para prueba tecnica.

### How do I get set up? ###

* Instalacion de ambientes (Spring boot, NodeJs, Eclipse, Visual Studio Code)
* Configuracion del cluster en mongodb 
* Configuracion del espacio de proyecto en spring boot
* Configuracion de dependencias para spring boot del mongodb
* Configuracion el proyecto en Visual Studio Code con Angular CLI 8

Web:
https://agenciaviajesfront.jaardiaz.now.sh/#/home