import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Touring app';
  isAdmin: boolean;

  constructor(private router: Router) { }

  btnHome() {
    this.router.navigate(['home']);
  }

  btnLogin() {
    this.router.navigate(['login']);
  }


}
