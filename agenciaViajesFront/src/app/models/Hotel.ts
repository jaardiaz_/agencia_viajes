import { Room } from './Room';

export class Hotel{
    id:string;
	name:string;
	city:string;
	country:string;
	departament:string;
	address:string;
	telephone:string;
	ranking:string;
	state:number;
	rooms: Array<Room>;
}