export class Person{
    id:string;
	name:string;
	lastName:string;
	gender:string;
	documentType:string;
	documentNumber:string;
	email:string;
	telephone:string;
}