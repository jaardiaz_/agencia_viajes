import { Person } from './Person';
import { ContactEmergency } from './contactEmergency';

export class Reservation{
    id:string;
	dateEntry:string;
	dateExit:string;
	totalPersons:number;
	persons:Array<Person>;
	contactEmergency:ContactEmergency;
}