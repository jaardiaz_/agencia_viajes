import { Reservation } from './Reservation';

export class Room{
    id:string;
	number:number;
	floor:number;
	type:string;
	capacity:number;
	description:string;
	price:number;
	taxes:number;
	state:number;
	reservations:Array<Reservation>;
}