export class ContactEmergency{
    id:number;
	name:string;
	lastName:string;
	telephone:string;
}