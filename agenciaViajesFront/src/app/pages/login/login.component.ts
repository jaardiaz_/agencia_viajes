import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';
import { User } from 'src/app/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private loginService: LoginService, private router: Router) {
    this.user = new User;
  }

  ngOnInit() {
    let currentUser = localStorage.getItem('currentUser');
    if(currentUser == 'admin'){
      this.router.navigate(['admin']);
    }
  }


  validate() {

    if (this.user.email == null || this.user.password == null || this.user.email == '' || this.user.password == '') {
      return true;
    }

  }

  login() {
    this.loginService.getUser(this.user.email, this.user.password)
      .subscribe(data => {
        this.processLogin(data);
      });
  }

  processLogin(userLogin: User) {

    if (userLogin == null) {
      alert("El usuario que acabas de ingresar no existe");
    } else {
      if (userLogin.type == "admin") {
        localStorage.setItem('currentUser', 'admin');
        this.router.navigate(['admin']);
      } else {
        this.router.navigate(['home']);
      }
    }

  }


}
