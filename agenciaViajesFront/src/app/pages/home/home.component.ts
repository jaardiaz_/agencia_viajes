import { Component, OnInit } from '@angular/core';
import { Hotel } from 'src/app/models/Hotel';
import { ReservationService } from 'src/app/services/reservation/reservation.service';
import { Room } from 'src/app/models/Room';
import { Person } from 'src/app/models/Person';
import { Reservation } from 'src/app/models/Reservation';
import { ContactEmergency } from 'src/app/models/contactEmergency';
import { element } from 'protractor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  hotels: Array<Hotel>;
  hotel: Hotel;
  room: Room;
  reservations: Array<Reservation>;
  persons: Array<Person>;
  person: Person;
  newReservation: Reservation;
  contactEmergency: ContactEmergency;
  filterDateEntry: string;
  filterDateExit: string;
  filterCity: string;
  filterTotalPerson: number;
  reservationVisible: boolean;

  constructor(private reservationService: ReservationService) {
    this.newReservation = new Reservation();
    this.person = new Person();
    this.persons = new Array();
    this.contactEmergency = new ContactEmergency();
    this.reservations = new Array();
    this.reservationVisible = false;
  }

  validateFilters() {

    if (this.filterDateEntry == null || this.filterDateExit == null || this.filterTotalPerson == null || this.filterCity == '' || this.filterCity == null) {
      return true;
    }

  }

  validateReservation() {

    if (this.validateFilters()) {
      return true;
    }

    if (this.persons == null || this.persons.length == 0) {
      return true;
    }

    if (this.contactEmergency.name == null || this.contactEmergency.lastName == null || this.contactEmergency.telephone == null) {
      return true;
    }

  }

  validateAddPerson(){
    if(this.person.name == null || this.person.name == ''  || this.person.telephone == null 
    || this.person.email == '' || this.person.email == null || this.person.lastName == '' 
    || this.person.lastName == null || this.person.gender == '' || this.person.gender == null 
    || this.person.documentNumber == null ){
      return true;
    }

    if(this.persons.length >= this.room.capacity){
      return true;
    }
  }

  clear(){
    this.contactEmergency = new ContactEmergency();
    this.hotels = new Array();
    this.hotel = new Hotel;
  }

  ngOnInit() {
    
  }

  showHotels() {
    this.reservationService.getFilteredHotels(this.filterDateEntry, this.filterDateExit, this.filterCity, this.filterTotalPerson)
      .subscribe(data => { this.hotels = data});     
  }

  startReservation(hotel: Hotel, room: Room) {
    this.reservationVisible = true;
    this.hotel = hotel;
    this.room = room;
    
    this.createReservation();
  }

  closeAddReservation(){
  this.persons = new Array();
  this.person = new Person;
  this.reservationVisible = false;
  }

  createReservation() {
    this.newReservation.dateEntry = this.filterDateEntry;
    this.newReservation.dateExit = this.filterDateExit;
    this.newReservation.totalPersons = this.filterTotalPerson;
  }

  addPerson() {
    this.person.id = Math.random().toString(36).substr(2, 9);
    this.persons.push(this.person);
    this.person = new Person();
  }

  deletePerson(pers: Person){
    this.persons = this.persons.filter(element => element.id != pers.id);
  }

  finishReservation() {
    this.newReservation.persons = this.persons;
    this.newReservation.contactEmergency = this.contactEmergency;

    this.hotel.rooms.forEach(function (element, index) {
      if (element.id == this.room.id) {

        if(this.hotel.rooms[index].reservations == null){
          this.hotel.rooms[index].reservations = this.reservations;
        }

        this.hotel.rooms[index].reservations.push(this.newReservation);
      }
    }, this);

    this.updateHotel(this.hotel);
  }

  updateHotel(value: Hotel) {
    this.reservationService.updateHotel(value)
      .subscribe(error => {
          console.log(<any>error);
        }
      );
      this.sendEmail();
      this.clear();
      this.reservationVisible = false;
  }

  sendEmail(){
    this.reservationService.sendEmail(this.persons)
      .subscribe(error => {
          console.log(<any>error);
        }
      );
  }
}
