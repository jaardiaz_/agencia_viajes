import { Component, OnInit } from '@angular/core';
import { HotelService } from 'src/app/services/hotel/hotel.service';
import { Hotel } from 'src/app/models/Hotel';
import { Room } from 'src/app/models/Room';
import { Reservation } from 'src/app/models/Reservation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  hotels: Array<Hotel>;
  hotel: Hotel;
  room: Room;
  rooms: Array<Room>;
  informationHotel: boolean;
  formRoom: boolean;
  btnEditRoom: boolean;
  showReservation: boolean;
  btnEditHotel: boolean;
  dataReservation: Array<Reservation>;

  constructor(private hotelsService: HotelService, private router: Router) {
    this.hotel = new Hotel();
    this.room = new Room();
    this.rooms = new Array();
    this.dataReservation = new Array();
  }

  validateRoom() {
    if (this.room.number == null || this.room.floor == null || this.room.price == null
      || this.room.state == null || this.room.taxes == null || this.room.type == null
      || this.room.capacity == null || this.room.description == '' || this.room == null) {
      return true;
    }
  }

  validateEditHotel() {
    if (this.hotel.name == null || this.hotel.name == '' || this.hotel.address == null
      || this.hotel.address == '' || this.hotel.city == null || this.hotel.country == null
      || this.hotel.departament == null || this.hotel.ranking == null
      || this.hotel.state == null || this.hotel.telephone == null || this.hotel.telephone == '') {
      return true;
    }
  }

  ngOnInit() {
    this.informationHotel = false;
    this.allHotels();
  }


  allHotels() {
    this.hotelsService.getAllHotels().subscribe(data => {
      this.hotels = data;
    });
  }

  showEditHotel(hotel: Hotel) {
    this.informationHotel = true;
    this.btnEditRoom = false;
    this.formRoom = false;
    this.btnEditHotel = true;
    this.hotel = hotel;
  }

  showAddHotel() {
    this.hotel = new Hotel;
    this.hotel.id = this.generateId();
    this.informationHotel = true;
    this.btnEditRoom = false;
    this.formRoom = false;
    this.btnEditHotel = false;
  }

  generateId() {
    return Math.random().toString(36).substr(2, 9);
  }

  showEditRoom(room: Room) {
    this.formRoom = true;
    this.btnEditRoom = true;
    this.room = room;
  }

  closeFormRoom() {
    this.formRoom = false;
    this.room = new Room;
  }

  closeEditHotel() {
    this.informationHotel = false;
    this.hotel = new Hotel;
    this.allHotels();
  }

  showAddRoom() {
    this.room = new Room;
    this.room.id = this.generateId();
    this.formRoom = true;
    this.btnEditRoom = false;
  }

  showReservations() {
    this.showReservation = true;
  }

  showDetailsReservation(dataReservation: Array<Reservation>) {
    this.dataReservation = dataReservation;
  }

  closeShowReservation() {
    this.showReservation = false;
  }

  addRoom() {
    if (this.hotel.rooms == null) {
      this.hotel.rooms = this.rooms;
    }
    this.hotel.rooms.push(this.room);
    this.room = new Room;
    this.formRoom = false;
  }

  editRoom() {
    this.hotel.rooms.forEach(function (element, index) {
      if (element.id == this.room.id) {
        this.hotel.rooms[index] = this.room;
      }
    }, this);
    this.room = new Room;
    this.formRoom = false;
  }

  saveChanges() {
    this.hotelsService.updateHotel(this.hotel).subscribe(error => {
      console.log(<any>error);
    });

    this.hotel = new Hotel;
    this.hotels = new Array();
    this.allHotels();
    this.informationHotel = false;
  }

  exit(){
    localStorage.clear();
  }

}
