import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hotel } from 'src/app/models/Hotel';
import { Person } from 'src/app/models/Person';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient) { }

  context = 'touringapp';
  url = 'https://app-jaardiaz.herokuapp.com/' + this.context;
  urlUpdateHotel = this.url + '/addHotel';
  urlGetHotels = this.url + '/filteredHotels';
  urlSendEmails = this.url + '/sendEmail';
  emails: Array<string>;

  getFilteredHotels(dateEntry, dateExit, city, capacity): Observable<any> {
    return this.http.get(this.urlGetHotels + '?dateEntry=' + dateEntry + '&dateExit=' + dateExit + '&city=' + city + '&capacity=' + capacity);
  }

  updateHotel(hotel: Hotel): Observable<any> {
    let json = JSON.stringify(hotel);
    return this.http.post(this.urlUpdateHotel, hotel);
  }

  sendEmail(persons: Array<Person>): Observable<any> {
    this.emails = new Array();
    persons.forEach(function (person) {
      this.emails.push(person.email);
    },this);
    return this.http.post(this.urlSendEmails, this.emails);
  }
}
