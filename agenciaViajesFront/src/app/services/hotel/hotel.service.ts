import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hotel } from 'src/app/models/Hotel';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) { }

  context = 'touringapp';
  url = 'https://app-jaardiaz.herokuapp.com/'+this.context;
  urlUpdateHotel = this.url+'/addHotel';
  urlGetHotels = this.url+'/allHotels';

getAllHotels(): Observable<any>{
  return this.http.get(this.urlGetHotels);
}

updateHotel(hotel: Hotel): Observable<any>{
  return this.http.post(this.urlUpdateHotel, hotel);
}

}
