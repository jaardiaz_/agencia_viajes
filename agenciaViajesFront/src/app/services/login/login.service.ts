import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CanActivate, Router } from '@angular/router';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements CanActivate {

  constructor(private http: HttpClient, private router: Router) { }

  context = 'touringapp';
  url = 'https://app-jaardiaz.herokuapp.com/' + this.context;
  urlLogin = this.url + '/login';


  getUser(email, password): Observable<any> {
    return this.http.get(this.urlLogin + '?email=' + email + '&password=' + password);
  }

  canActivate() {

    let user = this.getUserLoggedIn();

      if (!(user == 'admin')) {
        this.router.navigate(['home']);
        return false;
      }
       
    return true;
  }


  getUserLoggedIn() {
    return localStorage.getItem('currentUser');
  }


}
