package com.agenciaviajes.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.agenciaviajes.app.model.Hotel;
import com.agenciaviajes.app.services.HotelServices;

@CrossOrigin(origins = "*")
@RestController
public class HotelController {

	@Autowired
	private HotelServices hotelServices;

	@PostMapping("/addHotel")
	public void addHotel(@RequestBody Hotel hotel) {
		hotelServices.addHotel(hotel);
	}
	
	@PostMapping("/sendEmail")
	public void sendEmail(@RequestBody List<String> emails) {
		hotelServices.sendEmail(emails);
	}

	@GetMapping("/allHotels")
	public List<Hotel> allHotel() {
		return hotelServices.selectAllHotels();
	}

	@GetMapping("/filteredHotels")
	@ResponseBody
	public List<Hotel> filteredHotels(@RequestParam("dateEntry") String dateEntry,
			@RequestParam("dateExit") String dateExit, @RequestParam("city") String city, @RequestParam("capacity") int capacity) throws Exception {
		return hotelServices.filteredHotels(dateEntry, dateExit, city, capacity);
	}
	
	@GetMapping("/jaar")
	public String jaar() {
		return "jaar!";
	}

}
