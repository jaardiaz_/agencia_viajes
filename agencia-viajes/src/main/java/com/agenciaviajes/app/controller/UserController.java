package com.agenciaviajes.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.agenciaviajes.app.model.User;
import com.agenciaviajes.app.services.UserServices;

@CrossOrigin(origins = "*")
@RestController
public class UserController {
	@Autowired
	private UserServices userServices;

	@PostMapping("/addUser")
	public void addUser(@RequestBody User user) {
		userServices.addUser(user);
	}

	@GetMapping("/login")
	public User login(@RequestParam("email") String email, @RequestParam("password") String password) {

		return userServices.searchUser(email, password);

	}
}
