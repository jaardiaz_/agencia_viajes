package com.agenciaviajes.app.model;

import java.util.List;

import org.springframework.data.annotation.Id;

public class Reservation {

	@Id
	private String id;
	private String dateEntry;
	private String dateExit;
	private int totalPersons;
	private List<Person> persons;
	private ContactEmergency contactEmergency;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the dateEntry
	 */
	public String getDateEntry() {
		return dateEntry;
	}
	/**
	 * @param dateEntry the dateEntry to set
	 */
	public void setDateEntry(String dateEntry) {
		this.dateEntry = dateEntry;
	}
	/**
	 * @return the dateExit
	 */
	public String getDateExit() {
		return dateExit;
	}
	/**
	 * @param dateExit the dateExit to set
	 */
	public void setDateExit(String dateExit) {
		this.dateExit = dateExit;
	}
	/**
	 * @return the totalPersons
	 */
	public int getTotalPersons() {
		return totalPersons;
	}
	/**
	 * @param totalPersons the totalPersons to set
	 */
	public void setTotalPersons(int totalPersons) {
		this.totalPersons = totalPersons;
	}
	/**
	 * @return the persons
	 */
	public List<Person> getPersons() {
		return persons;
	}
	/**
	 * @param persons the persons to set
	 */
	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}
	/**
	 * @return the contactEmergency
	 */
	public ContactEmergency getContactEmergency() {
		return contactEmergency;
	}
	/**
	 * @param contactEmergency the contactEmergency to set
	 */
	public void setContactEmergency(ContactEmergency contactEmergency) {
		this.contactEmergency = contactEmergency;
	}

	

}
