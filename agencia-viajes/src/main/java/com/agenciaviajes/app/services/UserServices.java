package com.agenciaviajes.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agenciaviajes.app.model.User;
import com.agenciaviajes.app.repository.UserRepository;

@Service
public class UserServices {

	@Autowired
	private UserRepository userRepository;

	public void addUser(User user) {
		userRepository.save(user);
	}

	public User searchUser(String email, String password) {

		List<User> users = allUsers();

		if (!users.isEmpty()) {

			for (User user : users) {

				if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
					return user;
				}
			}
		}

		return null;
	}

	private List<User> allUsers() {
		return userRepository.findAll();
	}

}
