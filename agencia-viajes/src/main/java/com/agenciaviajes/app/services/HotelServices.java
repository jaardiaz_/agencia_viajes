package com.agenciaviajes.app.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.agenciaviajes.app.model.Hotel;
import com.agenciaviajes.app.model.Reservation;
import com.agenciaviajes.app.model.Room;
import com.agenciaviajes.app.repository.HotelRepository;

@Service
public class HotelServices {

	@Autowired
	private HotelRepository hotelRepository;
	@Autowired
	private JavaMailSender mailSender;

	public void addHotel(Hotel hotel) {

		hotelRepository.save(hotel);
	}

	public List<Hotel> selectAllHotels() {
		return allHotels();
	}

	public List<Hotel> filteredHotels(String dateEntry, String dateExit, String city, int capacity) throws Exception {

		List<Hotel> hotels = allHotels();
		List<Hotel> filteredHotels = new ArrayList<>();
		List<Room> filteredRoom = new ArrayList<>();
		Hotel filteredHotel;
		if (!hotels.isEmpty()) {
			for (Hotel hotel : hotels) {
				if (hotel.getCity().equals(city) && hotel.getState() == 1) {
					List<Room> rooms = hotel.getRooms();
					if (rooms != null) {
						for (Room room : rooms) {
							if (capacity <= room.getCapacity() && room.getState() == 1) {
								List<Reservation> reservations = room.getReservations();
								boolean availability = true;
								if (reservations != null) {
									for (Reservation reservation : reservations) {
										Date dateEntryNew = new SimpleDateFormat("dd-MM-yyyy").parse(dateEntry);
										Date dateEntryReserv = new SimpleDateFormat("dd-MM-yyyy")
												.parse(reservation.getDateEntry());
										Date dateExiReserv = new SimpleDateFormat("dd-MM-yyyy")
												.parse(reservation.getDateExit());
										if ((dateEntryNew.after(dateEntryReserv) && dateEntryNew.before(dateExiReserv))
												|| (dateEntryNew.compareTo(dateEntryReserv) == 0
														|| dateEntryNew.compareTo(dateExiReserv) == 0)) {
											availability = false;
										}
									}
								}
								if (availability) {
									filteredRoom.add(room);
								}

							}
						}
					}

					if (!(filteredRoom.isEmpty() || filteredRoom == null)) {
						filteredHotel = hotel;
						filteredHotel.setRooms(filteredRoom);
						filteredHotels.add(filteredHotel);
						filteredRoom = new ArrayList<>();
					}

				}
			}
		}

		return filteredHotels;
	}

	private List<Hotel> allHotels() {
		return hotelRepository.findAll();
	}

	public void sendEmail(List<String> emails) {
        SimpleMailMessage email = new SimpleMailMessage();
        
        for(String e : emails) {
        	
        	email.setTo(e);
            email.setSubject("Confirmación de reserva");
            email.setText("Por medio de este correo, hacemos notificación de tu reserva realizada en TouringApp.");
            mailSender.send(email);
        	
        	
        }
        

	}

}
