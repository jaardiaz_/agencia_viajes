package com.agenciaviajes.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.agenciaviajes.app.model.Hotel;

@Repository
public interface HotelRepository extends MongoRepository<Hotel, Integer> {

}
